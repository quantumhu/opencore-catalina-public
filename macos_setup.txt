# disable gatekeeper
sudo spctl --master-disable

# to get PciRoot name of GPU
./gfxutil -f GFX0

# disable hibernation
sudo pmset -a autopoweroff 0

# disable powernap
sudo pmset -a powernap 0

# disable sleep -> hibernation
sudo pmset -a standby 0

# disable iphone/iwatch wake
sudo pmset -a proximitywake 0

# USB controllers
IOService:/AppleACPIPlatformExpert/PCI0@0/AppleACPIPCI/GP13@8,1/IOPP/XHC0@0,3/XHC0@60000000
IOService:/AppleACPIPlatformExpert/PCI0@0/AppleACPIPCI/GPP2@1,3/IOPP/PTXH@0/PTXH@00000000
AppleUSBXHCIPCI
model iMacPro1,1

# run to check opencore version
nvram 4D1FDA02-38C7-4A6A-9CC6-4BCCA8B30102:opencore-version

# to re-enable debugging
# add -v to boot args under NVRAM
# set Misc -> Debug -> AppleDebug to True
# set Misc -> Debug -> Target to 67 from 3

# set chrome to white when system is dark mode
defaults write com.google.Chrome NSRequiresAquaSystemAppearance -bool Yes

# disable auto correct globally
defaults write -g WebAutomaticTextReplacementEnabled -bool false

# to mount EFI partition
diskutil list 	# find ur EFI partition
sudo mkdir /Volumes/EFI
sudo mount -t msdos /dev/<diskDxP> /Volumes/EFI
diskutil unmount /Volumes/EFI 	# to unmount

## Tryna fix sleep lol ##
# patch IRQ conflicts using SSDTTime tool
# put SSDT in ACPI folder, and edit config.plist

# audio layout ID more permanent, following command to find PciRoot of audio 
./gfxutil -f HDEF
# use this output and put under DeviceProperties -> Add
# layout-id = (Data) <01 00 00 00> , alcid = 1 equivalent
# finally remove alcid from boot args
# Enabled Kernel -> Quirks -> PowerTimeoutKernelPanic => True

# bruh the sleep is fixed when you put USB into non-XHC0 controller USB ports

# usb mouse works wonky in usb 2 port?? why

# I disabled SSDT-EC-USBX.aml and enabled SSDT-USBX.aml, idk the difference lol

# created SSDT-EC.aml using SSDTTime and enabled that, deleted SSDT-EC-USBX (because apparently this precompiled is bloated)

# Asus BT400 is Broadcom BCM20702A0 (according to IORegistryExplorer)
# no need to add more firmware because already supported (vendor ID: 0x0b05, device ID: 0x17cb)
# install BrcmBluetoothInjector.kext because Catalina, RAM3 requires this
# install BrcmFirmwareData.kext since we are doing bootloader injection
# install BrcmPatchRAM3.kext for 10.15
# install BT4LEContinuityFixup (also by acidanthera like above)
# inject in above order

# ok so you might need an actual broadcom wifi card too
# i'm getting a TP-Link Archer T6E AC1300 (Broadcom BCM4360 equivalent, vendor ID: 0x143c, device ID: 0x43a0)
# handoff shows up properly in macos settings
# HANDOFF WORKS
# had to add the device to Info.plist inside AirportBrcmFixup.kext since the vendor ID doesn't actually match broadcom (0x14e4)

# got airdrop to appear by creating a new user account (no apple ID login) then logging out, then back into original
# then deleting new user account

# sleep is broken by Bluetooth USB adapter in 3.1 (which doesn't break for USB mouse) so need to keep it in 2.0 for now
