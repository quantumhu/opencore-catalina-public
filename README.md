# MacOS Catalina 10.15.6 OpenCore 0.6.0 Setup #

It's working, yay!

### What's working: ###

Pretty much everything works, except:

* Sleep works, but only when you don't plug things into certain rear USB 2.0/3.0 ports (but 3.1 has no problem?).
    + Won't sleep if my USB 2.0 wireless mouse is in a 2.0 slot
    + Won't sleep if my USB 2.0 BT adapter is in a *3.0* slot
* I haven't bothered with updating MacOS (so can't confirm if it'll work).
* Features like proximity wake and power nap are turned off to prevent sleep issues.
* Did not test iMessage / Facetime because I don't use it.
* I removed all serial numbers so that will need to be [filled in yourself](https://github.com/corpnewt/GenSMBIOS).

### Kext List: ###

* AMDRyzenCPUPowerManagement
* AppleALC	- layout ID of 1
* AppleMCEReporterDisabler
* Lilu
* RealtekRTL8111
* SMCAMDProcessor
* USB-Map	- this is custom for my motherboard + case
* VirtualSMC
* WhateverGreen

### BIOS Options: ###

* Fast Boot: OFF
* Secure Boot: OFF
* Serial/Parallel Port: OFF
* CSM Support: OFF
* Above 4G Decoding: OFF (because we have npci=0x2000 in boot args)
* XHCI Hand-off: ON
* SATA Mode: AHCI
* SVM: ON
* RAM Overclock: set to 3200MHz

### PC Build: ###
* CPU: AMD Ryzen 5 3600
* GPU: ASUS RX 560 O4G-EVO
* Motherboard: Gigabyte B450 AORUS M
* Onboard Sound: ALC892 codec
* Onboard NIC: Realtek RTL8168
* Case: Fractal Design Focus G Mini
* RAM: 32GB DDR4, overclocked to 3200MHz
* Storage: Crucial MX500 500GB (Windows) and Kingston A400 256GB (MacOS)
* PSU: SeaSonic Focus GM 650W 80+ Gold
* WiFi Card: TP-Link Archer T6E AC1300
* Bluetooth: Asus BT400 USB Adapter

### Useful Commands: ###

Disable gatekeeper

`sudo spctl --master-disable`

Get PciRoot name of GPU

`./gfxutil -f GFX0`

Disable hibernation

`sudo pmset -a autopoweroff 0`

Disable powernap

`sudo pmset -a powernap 0`

Disable sleep -> hibernation

`sudo pmset -a standby 0`

Disable iPhone/iWatch wake

`sudo pmset -a proximitywake 0`

Find OpenCore version

`nvram 4D1FDA02-38C7-4A6A-9CC6-4BCCA8B30102:opencore-version`

Disable auto correct globally

`defaults write -g WebAutomaticTextReplacementEnabled -bool false`

Set Chrome to light mode when system is in dark mode

`defaults write com.google.Chrome NSRequiresAquaSystemAppearance -bool Yes`

### Miscellaneous: ###

To re-enable debugging for OpenCore, add -v to boot args under the NVRAM section and set Misc -> Debug -> AppleDebug to True and Misc -> Debug -> Target to 67 (from 3).

To mount the EFI partition:

`diskutil list                      # find your EFI partition`

`sudo mkdir /Volumes/EFI`

`sudo mount -t msdos /dev/<diskDxP> /Volumes/EFI`

`diskutil unmount /Volumes/EFI      # to unmount`

I had to modify Info.plist inside AirportBrcmFixup.kext to get the WiFi card to work because the kext is mostly for Broadcom devices. The TP-Link device doesn't have the right vendor ID so I added it in so that the kext would activate for my card. To get AirDrop to show up in Finder, I needed to follow these steps (I did not have the WiFi card installed when I first installed MacOS):

1. Create a new user account with no Apple ID login.
2. Log out.
3. Log into user account you intend to use.
4. Delete newly created account (if you want).

Some more specific details can be found inside macos_setup.txt.